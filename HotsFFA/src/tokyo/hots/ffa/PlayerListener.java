package tokyo.hots.ffa;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import tokyo.hots.ffa.entity.DroppedEntityRemoveRunnable;
import tokyo.hots.ffa.entity.FireRemoveTask;
import tokyo.hots.ffa.game.GameState;
import tokyo.hots.ffa.manager.PlayerInfoManager;
import tokyo.hots.ffa.util.ChatMessage;
import tokyo.hots.ffa.util.ItemUtil;

public class PlayerListener implements Listener{

	private FFA plugin;

	public PlayerListener(FFA plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e){
		if(!(e.getDamager() instanceof Player)){
			return;
		}
		Player damager = (Player) e.getDamager();
		if(plugin.getGame().getState() == GameState.ENDING){
			e.setCancelled(true);
			ChatMessage.send(damager, "Not PvP time");
			return;
		}
		Location center = plugin.getGame().getMap().getSpawn();
		if(damager.getLocation().distance(center) < 9){
			e.setCancelled(true);
			ChatMessage.send(damager, "Not PvP area");
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		PlayerInfo info = new PlayerInfo(player);
		plugin.getPlayerInfoManager().register(info);
		player.teleport(plugin.getGame().getMap().getSpawn());
		e.setJoinMessage(ChatMessage.PREFIX + ChatColor.GREEN + player.getName() + ChatColor.GRAY + " has joined the game");
		player.setHealth(20.0);
		player.setFoodLevel(20);
		plugin.getGameManager().setKit(player);
		player.setGameMode(GameMode.SURVIVAL);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player player = e.getPlayer();
		PlayerInfoManager manager = plugin.getPlayerInfoManager();
		manager.unregitser(manager.getPlayerInfo(player));
	}

	@EventHandler
	public void onKill(PlayerDeathEvent e){
		if(!(e.getEntity().getKiller() instanceof Player))return;
		if(plugin.getGame().getState() == GameState.ACTIVE){
			Player dead = e.getEntity();
			Player killer = dead.getKiller();
			e.setDeathMessage(ChatMessage.PREFIX + ChatColor.GREEN + dead.getName() + ChatColor.GRAY + " was killed by " + ChatColor.GREEN + killer.getName());

			PlayerInfo deadi = plugin.getPlayerInfoManager().getPlayerInfo(dead);
			deadi.setDeaths(deadi.getDeaths()+1);

			PlayerInfo killeri = plugin.getPlayerInfoManager().getPlayerInfo(killer);
			killeri.setKills(killeri.getKills()+1);
			e.setDroppedExp(0);
			e.getDrops().clear();
			killer.getInventory().addItem(ItemUtil.getItemStack(ChatColor.YELLOW + "Heal Gapple", Material.GOLDEN_APPLE, ""));
			killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 1, 1);
			ChatMessage.send(killer, ChatColor.YELLOW + "Kill +1");
			Damageable d = (Damageable) killer;
			int health = (int) d.getHealth();
			ChatMessage.send(dead, ChatColor.YELLOW + killer.getName() + "'s HP: " + ChatColor.RED + "♥" + health);
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent e){
		if(!(e.getEntity() instanceof Player))return;
		e.getDrops().clear();
		e.setDroppedExp(0);
	}

	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		e.setRespawnLocation(plugin.getGame().getMap().getSpawn());
		plugin.getGameManager().setKit(e.getPlayer());
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e){
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE){
			e.setCancelled(true);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onLaunch(ProjectileLaunchEvent e){
		if(!(e.getEntity().getShooter() instanceof Player)){
			return;
		}
		Player player = (Player) e.getEntity().getShooter();
		Location center = plugin.getGame().getMap().getSpawn();
		Projectile arrow = e.getEntity();
		if(player.getLocation().distance(center) < 9){
			e.setCancelled(true);
			ChatMessage.send(player, "Don't use here");
			return;
		}
		if(arrow.getLocation().distance(center) < 9){
			arrow.remove();
		}
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		if(e.getBlock().getType() == Material.FIRE){
			Location center = plugin.getGame().getMap().getSpawn();
			if(e.getPlayer().getLocation().distance(center) < 9){
				e.setCancelled(true);
				ChatMessage.send(e.getPlayer(), "Don't use flint and steel here");
				return;
			}
		}
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onIgnite(BlockIgniteEvent e){
		if(e.getCause() ==  IgniteCause.FLINT_AND_STEEL){
			Block b = e.getBlock();
			new FireRemoveTask(b).runTaskLater(plugin, 20*5);
		}
	}

	@EventHandler
	public void onChange(FoodLevelChangeEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onSpawn(ItemSpawnEvent e){
		Entity item = e.getEntity();
		new DroppedEntityRemoveRunnable(item).start();
	}
}
