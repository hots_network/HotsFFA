package tokyo.hots.ffa;

import org.bukkit.entity.Player;

public class PlayerInfo {

	private Player player;

	private int kills, deaths;

	public PlayerInfo(Player player) {
		this.player = player;
		this.kills = 0;
		this.deaths = 0;
	}

	public Player getPlayer() {
		return player;
	}

	public int getKills() {
		return kills;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}






}
