package tokyo.hots.ffa;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import tokyo.hots.ffa.game.GameState;
import tokyo.hots.ffa.manager.PlayerInfoManager;

public class Sidebar {

	private FFA plugin;

	public Sidebar(FFA plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	public void setSidebar(){
		for(Player player : Bukkit.getOnlinePlayers()){
			ScoreboardManager manager = Bukkit.getScoreboardManager();
			Scoreboard board = manager.getNewScoreboard();
			Objective obj = board.registerNewObjective("yajuu", "dummy");

			obj.setDisplayName("" + ChatColor.AQUA + ChatColor.BOLD + "FFA");
			obj.setDisplaySlot(DisplaySlot.SIDEBAR);

			PlayerInfoManager imanager = plugin.getPlayerInfoManager();

			if(plugin.getGame().getState() == GameState.ACTIVE){

				Score a = obj.getScore("Time: " + ChatColor.GREEN + timeFormat(plugin.getGame().getTime()));
				a.setScore(-1);
				Score b = obj.getScore("Kill: " + ChatColor.GREEN + plugin.getPlayerInfoManager().getPlayerInfo(player).getKills());
				b.setScore(-2);
				Score k = obj.getScore("Death: " + ChatColor.GREEN + plugin.getPlayerInfoManager().getPlayerInfo(player).getDeaths());
				k.setScore(-3);
				Score on = obj.getScore("Players: " + ChatColor.GREEN + Bukkit.getOnlinePlayers().length);
				on.setScore(-4);

				Score c = obj.getScore(" ");
				c.setScore(-5);

				Score d = obj.getScore("" + ChatColor.YELLOW + ChatColor.BOLD + "1st" + ChatColor.GRAY + " (" + imanager.getRankingPlayerKills(1) + ")");
				d.setScore(-6);
				Score e = obj.getScore(imanager.getRankingPlayerName(1));
				e.setScore(-7);


				Score f = obj.getScore("" + ChatColor.GOLD + ChatColor.BOLD + "2nd" + ChatColor.GRAY + " (" + imanager.getRankingPlayerKills(2) + ")");
				f.setScore(-8);
				Score g = obj.getScore(imanager.getRankingPlayerName(2));
				g.setScore(-9);


				Score h = obj.getScore("" + ChatColor.RED + ChatColor.BOLD + "3rd" + ChatColor.GRAY + " (" + imanager.getRankingPlayerKills(3) + ")");
				h.setScore(-10);
				Score i = obj.getScore(imanager.getRankingPlayerName(3));
				i.setScore(-11);

				Score aa = obj.getScore("     ");
				aa.setScore(-12);

				Score bb = obj.getScore(ChatColor.YELLOW + "Hots Network");
				bb.setScore(-13);

				player.setScoreboard(board);
			}
			if(plugin.getGame().getState() == GameState.ENDING){
				Score a = obj.getScore("Next in " + plugin.getGame().getTime());
				a.setScore(-1);
				player.setScoreboard(board);
			}
		}
	}

	public String timeFormat(int sc){
		float fun = sc / 60;
		float byou = sc % 60;
		int funn = (int)Math.floor(fun);
		int byouu = (int)byou; String funnn = String.valueOf(funn);
		String byouuu = String.valueOf(byouu);
		int funkazu = funnn.length();
		int byoukazu = byouuu.length();
		if (funkazu == 1) {
	      funnn = "0" + funnn;
	    }
	    if (byoukazu == 1) {
	      byouuu = "0" + byouuu;
	    }
	    String time = funnn + ":" + byouuu;

	    return time;
	  }
}


	//    FFA
	// Time: 00:00
	// You: 1
	//
	//1st
	//testplayer1
	//2nd
	//testplayer2
	//3rd
	//testplayer3
	//
