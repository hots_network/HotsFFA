package tokyo.hots.ffa.entity;

import org.bukkit.entity.Entity;

import tokyo.hots.ffa.FFA;

public class DroppedEntityRemoveRunnable extends DroppedEntity {

	public DroppedEntityRemoveRunnable(Entity entity) {
		super(entity);
	}

	public void start(){
		this.runTaskLater(FFA.getPlugin(), 20*10);
	}

	@Override
	public void run() {
		if(getEntity() == null){
			this.cancel();
			return;
		}
		remove();
	}

}
