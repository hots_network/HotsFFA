package tokyo.hots.ffa.entity;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

public class FireRemoveTask extends BukkitRunnable {

	private Block fire;

	public FireRemoveTask(Block fire) {
		this.fire = fire;
	}

	@Override
	public void run() {
		if(fire.getType() == Material.AIR){
			this.cancel();
			return;
		}
		this.fire.setType(Material.AIR);
	}
}