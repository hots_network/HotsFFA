package tokyo.hots.ffa.entity;

import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class DroppedEntity extends BukkitRunnable  {

	private Entity entity;

	public DroppedEntity(Entity entity) {
		this.entity = entity;
	}

	public Entity getEntity() {
		return entity;
	}

	public void remove(){
		if(entity != null){
			entity.remove();
		}
	}


}