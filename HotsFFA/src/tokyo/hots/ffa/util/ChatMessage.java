package tokyo.hots.ffa.util;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatMessage {

	public static final String PREFIX = "" + ChatColor.AQUA + ChatColor.BOLD + "FFA" + ChatColor.GRAY + "》" + ChatColor.RESET;

	public static void send(CommandSender sender, String message){
		sender.sendMessage(PREFIX + message);
	}

	public static void send(Player player, String message){
		player.sendMessage(PREFIX + message);
	}
}
