package tokyo.hots.ffa.util;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemUtil {

	public static ItemStack getItemStack(String name, Material mat, String... lore){
		ItemStack is = new ItemStack(mat);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		if (lore != null) {
			meta.setLore(Arrays.asList(lore));
		}
		meta.spigot().setUnbreakable(true);
		is.setItemMeta(meta);
		return is;
	}
}