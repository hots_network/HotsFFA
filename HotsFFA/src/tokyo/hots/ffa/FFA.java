package tokyo.hots.ffa;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

import net.minecraft.util.com.google.common.collect.Lists;
import tokyo.hots.ffa.game.Game;
import tokyo.hots.ffa.manager.GameManager;
import tokyo.hots.ffa.manager.MapManager;
import tokyo.hots.ffa.manager.PlayerInfoManager;
import tokyo.hots.ffa.util.ItemUtil;

public class FFA extends JavaPlugin{

	private static FFA plugin;

	private Game game;

	private MapManager mapManager;

	private PlayerInfoManager playerInfoManager;

	private GameManager gameManager;

	private Sidebar sidebar;

	private List<ItemStack> kitItems;

	public void onEnable() {
		plugin = this;
		kitItems = Lists.newArrayList();
		initKit();
		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), this);
		this.getCommand("ffa").setExecutor(new Commands(this));
		Bukkit.getPluginManager().removePermission(new Permission("ffa.use.cmd"));
		this.mapManager = new MapManager(this);
		mapManager.loadMaps();
		this.playerInfoManager = new PlayerInfoManager(this);
		this.gameManager = new GameManager(this);
		this.game = new Game(this);
		this.sidebar = new Sidebar(this);
		game.start();
	}

	public void initKit(){
		kitItems.add(ItemUtil.getItemStack(ChatColor.YELLOW + "Stone Sword", Material.STONE_SWORD, new String[0]));
		kitItems.add(ItemUtil.getItemStack(ChatColor.YELLOW + "Bow", Material.BOW, new String[0]));
		kitItems.add(ItemUtil.getItemStack(ChatColor.YELLOW + "Fishing Rod", Material.FISHING_ROD, new String[0]));
		ItemStack is = new ItemStack(Material.FLINT_AND_STEEL);
		is.setDurability((short)62);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(ChatColor.YELLOW + "Flint and Steel");
		is.setItemMeta(meta);
		kitItems.add(is);
		ItemStack bow = new ItemStack(Material.ARROW, 16);
		kitItems.add(bow);
	}

	public List<ItemStack> getKitItems() {
		return kitItems;
	}

	public static FFA getPlugin() {
		return plugin;
	}

	public Game getGame() {
		return game;
	}

	public MapManager getMapManager() {
		return mapManager;
	}

	public static String getPluginFolder(){
		return plugin.getDataFolder().getAbsolutePath();
	}

	public PlayerInfoManager getPlayerInfoManager() {
		return playerInfoManager;
	}

	public GameManager getGameManager() {
		return gameManager;
	}

	public Sidebar getSidebar() {
		return sidebar;
	}
}