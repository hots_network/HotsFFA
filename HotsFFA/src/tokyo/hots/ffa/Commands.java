package tokyo.hots.ffa;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import tokyo.hots.ffa.util.ChatMessage;

public class Commands implements CommandExecutor{

	private FFA plugin;

	public Commands(FFA plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length == 0){
			ChatMessage.send(sender, "/ffa create <map>");
			ChatMessage.send(sender, "/ffa setspawn <map>");
			return false;
		}
		if(args.length == 2){
			Player player = (Player) sender;
			switch (args[0]) {
			case "create":
				plugin.getMapManager().create(sender, args[1]);
				break;
			case "setspawn":
				plugin.getMapManager().setspawn(player, args[1]);
				break;
			default:
				break;
			}
		}
		return false;
	}
}
