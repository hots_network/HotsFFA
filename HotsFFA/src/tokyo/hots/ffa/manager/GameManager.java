package tokyo.hots.ffa.manager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import tokyo.hots.ffa.FFA;
import tokyo.hots.ffa.PlayerInfo;
import tokyo.hots.ffa.util.ItemUtil;

public class GameManager {

	private FFA plugin;

	public GameManager(FFA plugin) {
		this.plugin = plugin;
	}

	@SuppressWarnings("deprecation")
	public void teleportAll(){
		for(Player player : Bukkit.getOnlinePlayers()){
			player.teleport(plugin.getGame().getMap().getSpawn());
		}
	}

	@SuppressWarnings("deprecation")
	public void sendResult(){
		PlayerInfoManager manager = plugin.getPlayerInfoManager();
		for(Player player : Bukkit.getOnlinePlayers()){
			PlayerInfo info = plugin.getPlayerInfoManager().getPlayerInfo(player);
			player.sendMessage(ChatColor.GRAY + "-----------------------------");
			player.sendMessage(player.getName() + "'s stats (戦績)");
			player.sendMessage(ChatColor.WHITE + "Kills (キル): " + ChatColor.AQUA + info.getKills());
			player.sendMessage(ChatColor.WHITE + "Deaths (デス): " + ChatColor.AQUA + info.getDeaths());
			player.sendMessage(ChatColor.WHITE + "Rank (全体の順位): " + ChatColor.AQUA + "#" +  plugin.getPlayerInfoManager().getRank(player));
			player.sendMessage(ChatColor.GRAY + "-----------------------------");
			player.sendMessage(" ");
			player.sendMessage(ChatColor.GRAY + "-----------------------------");
			player.sendMessage("" + ChatColor.YELLOW + ChatColor.BOLD + "1st - " + ChatColor.WHITE + manager.getRankingPlayerName(1));
			player.sendMessage("" + ChatColor.GOLD + ChatColor.BOLD + "2nd - " + ChatColor.WHITE + manager.getRankingPlayerName(2));
			player.sendMessage("" + ChatColor.RED + ChatColor.BOLD + "3rd - " + ChatColor.WHITE + manager.getRankingPlayerName(3));
			player.sendMessage(ChatColor.GRAY + "-----------------------------");
			player.playSound(player.getLocation(), Sound.LEVEL_UP, 1, 1);
		}
	}

	public void setKit(Player player){
		Inventory inv = player.getInventory();
		inv.clear();
		player.getEquipment().setHelmet(null);
		player.getEquipment().setChestplate(null);
		player.getEquipment().setLeggings(null);
		player.getEquipment().setBoots(null);
		for(int i = 0; i < plugin.getKitItems().size(); i++){
			ItemStack is = plugin.getKitItems().get(i);
			inv.setItem(i, is);
		}
		player.getEquipment().setHelmet(ItemUtil.getItemStack(ChatColor.YELLOW + "Iron Helmet", Material.IRON_HELMET, new String[0]));
		player.getEquipment().setChestplate(ItemUtil.getItemStack(ChatColor.YELLOW + "Iron Chestplate", Material.IRON_CHESTPLATE, new String[0]));
		player.getEquipment().setLeggings(ItemUtil.getItemStack(ChatColor.YELLOW + "Iron Leggings", Material.IRON_LEGGINGS, new String[0]));
		player.getEquipment().setBoots(ItemUtil.getItemStack(ChatColor.YELLOW + "Iron Boots", Material.IRON_BOOTS, new String[0]));
	}
}
