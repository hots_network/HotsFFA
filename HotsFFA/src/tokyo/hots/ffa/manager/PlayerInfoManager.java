package tokyo.hots.ffa.manager;

import java.util.List;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import tokyo.hots.ffa.FFA;
import tokyo.hots.ffa.PlayerInfo;

public class PlayerInfoManager {

	private FFA plugin;

	private List<PlayerInfo> players;

	public PlayerInfoManager(FFA plugin) {
		this.players = Lists.newArrayList();
		this.plugin = plugin;
	}

	public PlayerInfo getPlayerInfo(Player player){
		for(PlayerInfo info : players){
			if(info.getPlayer().equals(player)){
				return info;
			}
		}
		return null;
	}

	public void reset(PlayerInfo info){
		info.setKills(0);
		info.setDeaths(0);
	}

	public void resetAllPlayers(){
		for(PlayerInfo info : players){
			reset(info);
		}
	}

	public int getRank(Player player){
		for(int i = 0; i < players.size(); i++){
			if(players.get(i).getPlayer().equals(player)){
				i++;
				return i;
			}
		}
		return 0;
	}

	public String checkLength(String name){
		if(name.length() > 16){
			return name.substring(17, name.length());
		}
		return name;
	}

	public Player getRankingPlayer(int rank){
		switch (rank) {
		case 1:
			return getPlayers().get(0).getPlayer();
		case 2:
			return getPlayers().get(1).getPlayer();
		case 3:
			return getPlayers().get(2).getPlayer();
		default:
			break;
		}
		return null;
	}

	public int getRankingPlayerKills(int rank){
		if(players.size() < 3)return 0;
		switch (rank) {
		case 1:
			return getPlayerInfo(getRankingPlayer(1)).getKills();
		case 2:
			return getPlayerInfo(getRankingPlayer(2)).getKills();
		case 3:
			return getPlayerInfo(getRankingPlayer(3)).getKills();
		default:
			break;
		}
		return 0;
	}

	public String getRankingPlayerName(int rank){
		if(players.size() < 3)return " ";
		switch (rank) {
		case 1:
			String name = getRankingPlayer(1).getName();
			if(name == null)return "unknown.1st";
			return checkLength(name);
		case 2:
			String name1 = getRankingPlayer(2).getName();
			if(name1 == null)return "unknown.3rd";
			return checkLength(name1);
		case 3:
			String name2 = getRankingPlayer(3).getName();
			if(name2 == null)return "unknown.3rd";
			return checkLength(name2);
		default:
			break;
		}
		return null;
	}

	public void register(PlayerInfo info){
		players.add(info);
	}

	public void unregitser(PlayerInfo info){
		players.remove(info);
	}

	public List<PlayerInfo> getPlayers() {
		return players;
	}
}
