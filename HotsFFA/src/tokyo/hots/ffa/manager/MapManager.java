package tokyo.hots.ffa.manager;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import tokyo.hots.ffa.FFA;
import tokyo.hots.ffa.Map;
import tokyo.hots.ffa.util.ChatMessage;
import tokyo.hots.ffa.util.YamlEditor;

public class MapManager {

	private FFA plugin;

	private List<Map> maps;

	private String dir;

	public MapManager(FFA plugin) {
		this.plugin = plugin;
		this.maps = Lists.newArrayList();
		this.dir = FFA.getPluginFolder()  + "/maps/";
	}

	public void loadMaps(){
		File dir2 = new File(dir);
		String[] list = dir2.list();
		if (list != null){
			String[] arrayOfString1;
			int j = (arrayOfString1 = list).length;
			for (int i = 0; i < j; i++){
				String filename = arrayOfString1[i];
				YamlEditor edit = new YamlEditor(new File(this.dir + filename));
				filename = filename.substring(0, filename.indexOf(".yml"));
				maps.add(new Map(filename, edit.getLocation("Spawn.World", "Spawn.X", "Spawn.Y", "Spawn.Z", "Spawn.Yaw", "Spawn.Pitch")));
			}
		}
	}

	public Map randomMap(){
		Collections.shuffle(maps);
		return maps.get(0);
	}

	public void create(CommandSender sender, String name) {
		File file = new File(dir + name + ".yml");
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		ChatMessage.send(sender, "Successfully. New map has created.");
	}

	public void setspawn(Player player, String name){
		File file = new File(dir + name + ".yml");
		YamlEditor edit = new YamlEditor(file);
		Location loc = player.getLocation();
		edit.setValue("Spawn.World", player.getWorld().getName());
		edit.setValue("Spawn.X", loc.getX());
		edit.setValue("Spawn.Y", loc.getY());
		edit.setValue("Spawn.Z", loc.getZ());
		edit.setValue("Spawn.Yaw", loc.getPitch());
		edit.setValue("Spawn.Pitch", loc.getPitch());
		edit.save();
		ChatMessage.send(player, "Successfully. Spawn set.");
	}
}
