package tokyo.hots.ffa.game;

import java.util.Collections;
import java.util.Comparator;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import tokyo.hots.ffa.FFA;
import tokyo.hots.ffa.Map;
import tokyo.hots.ffa.PlayerInfo;

public class Game extends BukkitRunnable{

	private FFA plugin;

	private Map map;

	private int time;

	private GameState state;

	public Game(FFA plugin) {
		this.plugin = plugin;
		this.map = plugin.getMapManager().randomMap();
		this.time = 420;
		this.state = GameState.ACTIVE;
	}

	public Map getMap() {
		return map;
	}

	public int getTime() {
		return time;
	}

	public void start(){
		this.runTaskTimer(plugin, 0, 20);
	}

	public GameState getState() {
		return state;
	}

	public void setState(GameState state) {
		this.state = state;
	}

	public void setTime(int time) {
		this.time = time;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		if(time != 0){
			time --;
			plugin.getSidebar().setSidebar();
			Collections.sort(plugin.getPlayerInfoManager().getPlayers(), new Comparator<PlayerInfo>() {@Override public int compare(PlayerInfo o1, PlayerInfo o2) {return o1.getKills() > o2.getKills() ? -1 : 1;}});
		}
		if(time == 0 || time <= 0){
			if(state == GameState.ACTIVE){
				plugin.getGameManager().sendResult();
				setState(GameState.ENDING);
				setTime(10);
				for(Player player : Bukkit.getOnlinePlayers()){
					player.getInventory().clear();
					player.getEquipment().setHelmet(null);
					player.getEquipment().setChestplate(null);
					player.getEquipment().setLeggings(null);
					player.getEquipment().setBoots(null);
				}
				return;
			}
			if(state == GameState.ENDING){
				plugin.getPlayerInfoManager().resetAllPlayers();
				map = plugin.getMapManager().randomMap();
				plugin.getGameManager().teleportAll();
				for(Player player : Bukkit.getOnlinePlayers()){
					plugin.getGameManager().setKit(player);
					player.setHealth(20.0);
				}
				setTime(420);
				setState(GameState.ACTIVE);
				return;
			}
		}
	}
}
